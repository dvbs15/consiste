package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import entities.Pessoa;

public class Program {

	public static void main(String[] args) {

		String caminhoArquivo = "C:\\temp\\Consiste\\eclipse-Consiste\\consiste\\IMC\\dataset.CSV";
		
		List<Pessoa> list = new ArrayList<>();

		File arquivo = new File(caminhoArquivo);
		String caminhoPasta = arquivo.getParent();
		String novoArquivo = caminhoPasta + "\\Daniel Vilas Boas de Souza.txt";

		try (BufferedReader br = new BufferedReader(new FileReader(caminhoArquivo))) {

			String linha = br.readLine();
			linha = br.readLine();
			while (linha != null) {
				
				String[] coluna = linha.split(";", 4);
				String firstName = coluna[0].trim().replaceAll("  "," ");
				String lastName = coluna[1].trim().replaceAll("   "," ").replaceAll("  "," ");
				double peso = coluna[2].length() > 0 ? Double.parseDouble(coluna[2].replaceAll(",", ".")) : 0.0;
				double altura = coluna[3].length() > 0 ? Double.parseDouble(coluna[3].replaceAll(",", ".")) : 0.0;
				if (peso < altura) {
					double trocador = peso;
					peso = altura;
					altura = trocador;
				}
				Pessoa imc = new Pessoa(firstName, lastName, peso, altura);
				list.add(imc);
	
				linha = br.readLine();
			}
			
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(novoArquivo))) {
				for (Pessoa pessoa : list) {
					bw.write(pessoa.toString());
					bw.newLine();
					System.out.println(pessoa.toString());
				}
			} catch (IOException e) {
				System.out.println("Erro ao escrever a pasta: " + e.getMessage());
			}

		} catch (IOException e) {
			System.out.println("Erro ao escrever o arquivo: " + e.getMessage());
		}
	}

}

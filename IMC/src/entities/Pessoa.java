package entities;

public class Pessoa {
	
	private String firstName;
	private String lastName;
	private Double peso;
	private Double altura;
	
	public Pessoa() {
		
	}
	
	public Pessoa(String firstName, String lastName, Double peso, Double altura) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.peso = peso;
		this.altura = altura;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public Double getAltura() {
		return altura;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}
	
	public double valorImc(){
		return peso/(Math.pow(altura, 2));
		
	}

	@Override
	public String toString() {
		return firstName.toUpperCase() + " " + lastName.toUpperCase() + " " + String.format("%.2f", valorImc());
	}

}
